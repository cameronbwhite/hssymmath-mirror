-- Copyright © 2013 Cameron White 
-- All rights reserved.
-- 
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions
-- are met:
-- 1. Redistributions of source code must retain the above copyright
--    notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright
--    notice, this list of conditions and the following disclaimer in the
--    documentation and/or other materials provided with the distribution.
-- 3. Neither the name of the project nor the names of its contributors
--    may be used to endorse or promote products derived from this software
--    without specific prior written permission.
-- 
-- THIS SOFTWARE IS PROVIDED BY THE PROJECT AND CONTRIBUTORS ``AS IS'' AND
-- ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED.  IN NO EVENT SHALL THE PROJECT OR CONTRIBUTORS BE LIABLE
-- FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
-- DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
-- OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
-- HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
-- LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
-- OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
-- SUCH DAMAGE.
import Prelude hiding (and, or)
import Text.ParserCombinators.Parsec
import System.Environment
import Data.Set (Set, toList, fromList, singleton, union, size)
import Text.Printf

data LogicExp
    = Atom Char
    | Not LogicExp
    | Or LogicExp LogicExp
    | Xor LogicExp LogicExp
    | And LogicExp LogicExp
    | Implies LogicExp LogicExp
    | Iff LogicExp LogicExp
    | Nand LogicExp LogicExp
    | Nor LogicExp LogicExp

instance Show LogicExp where
    show (Atom a) = [a]
    show (Not a) = printf "~%s" (show a)
    show (Or a b) = printf "(%s+%s)" (show a) (show b)
    show (Xor a b) = printf "(%s⊕%s)" (show a) (show b)
    show (And a b) = printf "(%s&%s)" (show a) (show b)
    show (Implies a b) = printf "(%s->%s)" (show a) (show b)
    show (Iff a b) = printf "(%s<->%s)" (show a) (show b)
    show (Nand a b) = printf "(%s|%s)" (show a) (show b)
    show (Nor a b) = printf "(%s/%s)" (show a) (show b)

instance Eq LogicExp where
    (==) a b = all (\(x,y) -> x == y) values
        where atoms = union (getAtoms a) (getAtoms b);
              n = size atoms;
              chars = toList atoms;
              xss = map (zip chars) (perm n [True, False]);
              values = map (\xs -> (evaluate a xs, evaluate b xs)) xss

    (/=) a b = not $ a == b

toNand :: LogicExp -> LogicExp
toNand (Atom a) = (Atom a)
toNand (Not a) = a' `Nand` a'
    where a' = toNand a
toNand (Or a b) = (a' `Nand` a') `Nand` (b' `Nand` b')
    where a' = toNand a;
          b' = toNand b
toNand (And a b) = (a' `Nand` b') `Nand` (a' `Nand` b')
    where a' = toNand a;
          b' = toNand b
toNand (Nand a b) = a' `Nand` b'
    where a' = toNand a;
          b' = toNand b
toNand (Nor a b) = ((a' `Nand` a') `Nand` (b' `Nand` b')) `Nand` 
                   ((a' `Nand` a') `Nand` (b' `Nand` b'))
    where a' = toNand a;
          b' = toNand b
toNand (Implies a b) = toNand $ Or (toNand $ Not a) b
toNand (Iff a b) = toNand $ (a `Implies` b) `And` (b `Implies` a)
    
toNor :: LogicExp -> LogicExp
toNor (Atom a) = (Atom a)
toNor (Not a) = a' `Nor` a'
    where a' = toNor a
toNor (Or a b) = (a' `Nor` b') `Nor` (a' `Nor` b')
    where a' = toNor a;
          b' = toNor b
toNor (And a b) = (a' `Nor` a') `Nor` (b' `Nor` b')
    where a' = toNor a;
          b' = toNor b
toNor (Nor a b) = a' `Nor` b'
    where a' = toNor a;
          b' = toNor b
toNor (Nand a b) = ((a' `Nor` a') `Nor` (b' `Nor` b')) `Nor` 
                   ((a' `Nor` a') `Nor` (b' `Nor` b'))
    where a' = toNor a;
          b' = toNor b
toNor (Implies a b) = toNor $ Or (toNor $ Not a) b
toNor (Iff a b) = toNor $ (a `Implies` b) `And` (b `Implies` a)

parseAtom :: Parser LogicExp
parseAtom = do
    x <- letter
    return $ Atom x

parseUnary :: (LogicExp -> LogicExp) -> String -> Parser LogicExp
parseUnary exp str = do
    string str
    x <- parseExpr
    return $ exp x

parseBinary :: (LogicExp -> LogicExp -> LogicExp) -> String -> Parser LogicExp
parseBinary exp str = do
    spaces
    char '('
    spaces
    x <- parseExpr
    spaces
    string str
    spaces
    y <- parseExpr
    spaces
    char ')'
    spaces
    return $ exp x y
    
parseNot :: Parser LogicExp
parseNot = parseUnary Not "~"

parseOr :: Parser LogicExp
parseOr = parseBinary Or "+"

parseXor :: Parser LogicExp
parseXor = parseBinary Xor "⊕"

parseAnd :: Parser LogicExp
parseAnd = parseBinary And "&"
    
parseNand :: Parser LogicExp
parseNand = parseBinary Nand "|"

parseNor :: Parser LogicExp
parseNor = parseBinary Nor "/"

parseIff :: Parser LogicExp
parseIff = parseBinary Iff "<->"

parseImplies :: Parser LogicExp
parseImplies = parseBinary Implies "->"

parseExpr :: Parser LogicExp
parseExpr = try parseNot 
        <|> try parseOr 
        <|> try parseAnd 
        <|> try parseNor 
        <|> try parseNand 
        <|> try parseXor 
        <|> try parseIff
        <|> try parseImplies
        <|> parseAtom

readExpr :: String -> LogicExp
readExpr input = case parse parseExpr "LogExp" input of 
    Right val -> val

evalutateBinary :: (Bool -> Bool -> Bool) -> LogicExp -> LogicExp -> 
                   [(Char, Bool)] -> Bool
evalutateBinary operator exp1 exp2 xs = exp1' `operator` exp2'
    where exp1' = evaluate exp1 xs;
          exp2' = evaluate exp2 xs
          
evaluate :: LogicExp -> [(Char, Bool)] -> Bool
evaluate (Atom a) ((c,v):xs) | a == c = v
                             | otherwise = evaluate (Atom a) xs
evaluate (Not a) xs = not $ evaluate a xs
evaluate (And a b) xs = evalutateBinary and a b xs
evaluate (Or a b) xs = evalutateBinary or a b xs
evaluate (Xor a b) xs = evalutateBinary xor a b xs
evaluate (Nand a b) xs = evalutateBinary nand a b xs
evaluate (Nor a b) xs = evalutateBinary nor a b xs
evaluate (Implies a b) xs = evalutateBinary implies a b xs
evaluate (Iff a b) xs = evalutateBinary iff a b xs

truthTable :: LogicExp -> IO()
truthTable exp = do 
    sequence_ [ printf "%-5s " $ show c | c <- chars ]
    printf "| %s\n" $ show exp
    mapM_ (truthTable' exp) xss
        where chars = toList $ getAtoms exp;
              n = length chars;  
              xss = map (zip chars) (perm n [True, False])

truthTable' :: LogicExp -> [(Char, Bool)] -> IO()
truthTable' exp line = do
    sequence_ [ printf "%-5s " $ show b | (_, b) <- line ]
    printf "| %-5s\n" value
        where value = show $ evaluate exp line

perm i xs | i > 0 = [ x:ys | x <- xs, ys <- perm (i-1) xs]
          | otherwise = [[]]

getAtoms :: LogicExp -> Set Char
getAtoms (Atom a) = singleton a
getAtoms (Not a) = getAtoms a
getAtoms (Nor a b) = getAtoms' a b
getAtoms (Nand a b) = getAtoms' a b
getAtoms (And a b) = getAtoms' a b
getAtoms (Or a b) = getAtoms' a b
getAtoms (Xor a b) = getAtoms' a b
getAtoms (Iff a b) = getAtoms' a b
getAtoms (Implies a b) = getAtoms' a b

getAtoms' :: LogicExp -> LogicExp -> Set Char
getAtoms' a b = union (getAtoms a) (getAtoms b)

and :: Bool -> Bool -> Bool
and True True = True
and _    _    = False

or :: Bool -> Bool -> Bool
or True _    = True
or _    True = True
or _    _    = False

implies :: Bool -> Bool -> Bool
implies True False = False
implies _    _     = True

iff :: Bool -> Bool -> Bool
iff True  True  = True
iff False False = True
iff _     _     = False

xor :: Bool -> Bool -> Bool
xor False False = False
xor True  True  = False
xor _     _     = True

nand :: Bool -> Bool -> Bool
nand x y = not (x `and` y)

nor :: Bool -> Bool -> Bool
nor x y = not (x `or` y)

main :: IO()
main = do
    args <- getArgs
    truthTable (readExpr (args !! 0))
