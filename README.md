HsSymMath
=========

Interactive mathematical languages written in haskell

## Logic.hs ##

Formal logic parser 

### Command line ###

Currently the command line will take a logical expression as an argument and print out its corresponding truth table.

```sh
$ ./Logic '~a'
'a'   | ~a
True  | False
False | True 

$ ./Logic '((b+c)&a)'
'a'   'b'   'c'   | ((b+c)&a)
True  True  True  | True 
True  True  False | True 
True  False True  | True 
True  False False | False
False True  True  | False
False True  False | False
False False True  | False
False False False | False
```

### Code ###

```haskell
> And (Atom 'a') (Not (Atom 'b'))
(a&~b)

> truthTable $ And (Atom 'a') (Not (Atom 'b'))
'a'   'b'   | (a&~b)
True  True  | False
True  False | True 
False True  | False
False False | False

> toNand $ And (Atom 'a') (Not (Atom 'b'))
((a|(b|b))|(a|(b|b)))

> toNor $ And (Atom 'a') (Not (Atom 'b'))
((a/a)/((b/b)/(b/b)))

> let exp = And (Atom 'a') (Not (Atom 'b'))
> exp == toNand exp
True
> exp == toNor exp
True
> exp == toNor (toNand exp)
True
```

## RegularExpressions ##

Regular expression parser

### Command line ###

Currently the command line will take a regular expression as an argement and print out the set of corresponding strings.

```sh
$ ./RegularExpressions a
a = ["a"]

$ ./RegularExpressions abc
abc = ["abc"]

./RegularExpressions '(abc+efg)'
(abc+efg) = ["abc","efg"]

$ ./RegularExpressions '(abc+efg)*'
(abc+efg)* = ["","abc","efg","abcabc","efgabc","abcefg","efgefg","abcabcabc","efgabcabc","abcefgabc"]
```

