-- Copyright © 2013 Cameron White 
-- All rights reserved.
-- 
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions
-- are met:
-- 1. Redistributions of source code must retain the above copyright
--    notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright
--    notice, this list of conditions and the following disclaimer in the
--    documentation and/or other materials provided with the distribution.
-- 3. Neither the name of the project nor the names of its contributors
--    may be used to endorse or promote products derived from this software
--    without specific prior written permission.
-- 
-- THIS SOFTWARE IS PROVIDED BY THE PROJECT AND CONTRIBUTORS ``AS IS'' AND
-- ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED.  IN NO EVENT SHALL THE PROJECT OR CONTRIBUTORS BE LIABLE
-- FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
-- DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
-- OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
-- HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
-- LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
-- OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
-- SUCH DAMAGE.
import Text.ParserCombinators.Parsec
import System.Environment

limit = 10

-- Writen by Tim Sheard (http://web.cecs.pdx.edu/~sheard/)
data RegExp
    = Epsilon
    | One Char
    | Union RegExp RegExp
    | Cat RegExp RegExp
    | Star RegExp

instance Show RegExp where
    show (One a) = [a]
    show (Epsilon) = ""
    show (Union a b) = "(" ++ show a ++ "+" ++ show b ++ ")"
    show (Cat a b) = show a ++ show b
    show (Star a) = show a ++ "*"

process :: RegExp -> [[Char]]
process Epsilon = [""]
process (One a) = [[a]]
process (Union a b) = process a ++ process b 
process (Cat a b) = [ x ++ y | x <- (process a), y <- (process b) ]
process (Star a) = star (process a)

star :: [String] -> [String]
star xs = take limit $ [] : [ x ++ ys | ys <- star xs, x <- xs]

parseOne :: Parser RegExp
parseOne = do
   x <- letter 
   return $ One x

parseCat :: Parser RegExp
parseCat = do
    x <- try parseUnion <|> parseOne
    y <- parseExpr
    return $ Cat x y

parseUnion :: Parser RegExp
parseUnion = do
    char '('
    x <- parseExpr
    char '+'
    y <- parseExpr
    char ')'
    return $ Union x y

parseStar :: Parser RegExp
parseStar = do
    x <- try parseCat <|> try parseUnion <|> parseOne
    char '*'
    return $ Star x

parseExpr :: Parser RegExp
parseExpr = try parseStar 
        <|> try parseCat
        <|> try parseUnion
        <|> parseOne

readExpr :: String -> String
readExpr input = case parse parseExpr "RegEx" input of 
    Left err -> "No match: " ++ show err
    Right val -> show val ++ " = " ++ show (process val)

main :: IO()
main = do
    args <- getArgs
    putStrLn (readExpr (args !! 0))
